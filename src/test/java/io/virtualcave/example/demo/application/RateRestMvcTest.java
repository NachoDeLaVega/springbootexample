package io.virtualcave.example.demo.application;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import io.virtualcave.example.demo.domain.repository.CurrencyRepository;
import io.virtualcave.example.demo.domain.repository.RateRepository;
import io.virtualcave.example.demo.domain.service.DomainRateService;
import io.virtualcave.example.demo.domain.service.RateService;

@SpringBootTest
@AutoConfigureMockMvc
public class RateRestMvcTest {
	@Autowired
	private MockMvc mockMvc;
	

	@Test
	public void testUpdatePrice() throws Exception {
		this.mockMvc.perform(put("/rates/1/price/1200")).andDo(print()).andExpect(status().isOk());

	}
	@Test
	public void createRate() throws Exception {
		//TODO
	}
	@Test
	public void deleteRate() throws Exception {
		//TODO
	}
	@Test
	public void getRate() throws Exception {
		//TODO
	}
	@Test
	public void filter() throws Exception {
		//TODO
	}
	
}
