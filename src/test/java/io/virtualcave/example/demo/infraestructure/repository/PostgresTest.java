package io.virtualcave.example.demo.infraestructure.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.virtualcave.example.demo.domain.Rate;
import io.virtualcave.example.demo.infraestructure.RateDB;


public class PostgresTest {
	  private SpringDataPostgresRatesRepository repository;
	  private PostgresRatesRespository tested;

	    @BeforeEach
	    void setUp() {
	    	repository = mock(SpringDataPostgresRatesRepository.class);

	        tested = new PostgresRatesRespository(repository);
	    }

	    @Test
	    void findById() {
	        final int id = ThreadLocalRandom.current().nextInt(20, 30);
	        final RateDB r = new RateDB();
	        r.setId(id);	        
	        when(repository.findById(id)).thenReturn(Optional.of(r));

	        final Rate result = tested.findById(id);

	        assertEquals(r.toDomain(), result);
	    }
	    
}
