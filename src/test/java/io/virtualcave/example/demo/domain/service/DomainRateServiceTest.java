package io.virtualcave.example.demo.domain.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



import io.virtualcave.example.demo.domain.Rate;
import io.virtualcave.example.demo.domain.repository.CurrencyRepository;
import io.virtualcave.example.demo.domain.repository.RateRepository;

public class DomainRateServiceTest {
	   private RateRepository rateRepository;
	   private CurrencyRepository currencyRepository;
	   
	   private DomainRateService tested;

	    @BeforeEach
	    void setUp() {
	    	rateRepository = mock(RateRepository.class);
	    	currencyRepository = mock(CurrencyRepository.class);
	        tested = new DomainRateService(rateRepository, currencyRepository);
	    }

	    @Test
	    public void createRate() {
	    	Rate rate = new Rate();
	    	rate.setBrandId(1);
	    	rate.setProductId(1);
	    	rate.setPrice(1550);
	    	rate.getCurrency().setCode("EUR");
	    	when(rateRepository.create(any(Rate.class))).thenReturn(2);
	    	
	    	
	    	int result = tested.createRate(rate);
	    	
	        verify(rateRepository).create(any(Rate.class));
	        assertTrue(result > 0);
	    	
	    }
	    @Test
	    public void getRate() {	    
	    	//TODO
	    }
	    @Test
	    public void updatePrice() {	    
	    	//TODO
	    }
	    @Test
	    public void deleteRate() {	    
	    	//TODO
	    }
	    public void findRateByDate() {	    
	    	//TODO
	    }
	    
}
