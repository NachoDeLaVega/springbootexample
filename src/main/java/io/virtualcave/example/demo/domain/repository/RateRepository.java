package io.virtualcave.example.demo.domain.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import io.virtualcave.example.demo.domain.Rate;

public interface RateRepository {

	public Rate findById(int id);
	public int create(Rate rate);
	public boolean delete(int id);
	public boolean updatePrice(int id, int price);
	public List<Rate> findByDate(int brandId, int productId, Date date);
	
}
