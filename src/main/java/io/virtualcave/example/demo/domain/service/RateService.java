package io.virtualcave.example.demo.domain.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import io.virtualcave.example.demo.domain.Rate;

@Component
public interface RateService {

	public int createRate(Rate rate);
	public Rate getRate(int id);
	public boolean updatePrice(int id, int price);
	public boolean deleteRate(int id);
	public List<Rate> findRateByDate(int brandId, int productId, Date date);
		
	
	
}
