package io.virtualcave.example.demo.domain;

import java.util.Date;

public class Rate {
	private int id;
	private int brandId;
	private int productId;
	private Date startDate;
	private Date endDate;
	private int price;
	private Currency currency;

	public Rate() {
		startDate = new Date();
		endDate = new Date();
		currency = new Currency();

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBrandId() {
		return brandId;
	}

	public void setBrandId(int brandId) {
		this.brandId = brandId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public void setCurrency(String currency) {
		this.currency.setCode(currency);
		;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getPriceStr() {
		return String.format("%." + currency.getDecimals() + "f %s",
				(getPrice() / (Math.pow(10, currency.getDecimals()))), currency.getSymbol());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Rate))
			return false;
		Rate other = (Rate) obj;

		return (this.getId() == other.getId());
	}

}
