package io.virtualcave.example.demo.domain.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import io.virtualcave.example.demo.domain.Rate;
import io.virtualcave.example.demo.domain.repository.CurrencyRepository;
import io.virtualcave.example.demo.domain.repository.RateRepository;

@Component
public class DomainRateService implements RateService {

	private final RateRepository rateRepository;
	private final CurrencyRepository currencyRepository;

	public DomainRateService(final RateRepository rateRepository, final CurrencyRepository currencyRepository ) {
		this.currencyRepository = currencyRepository;
		this.rateRepository = rateRepository;
	}

	
	
	@Override
	public int createRate(Rate rate) {
		int result = rateRepository.create(rate);
		return result;
	}

	@Override
	public Rate getRate(int id) {
		Rate result = rateRepository.findById(id);;
		if (result!=null) {
			result.setCurrency(currencyRepository.findByCode(result.getCurrency().getCode()));
		}
		return result;
	}

	@Override
	public boolean updatePrice(int id, int price) {
		boolean result = false;	
		Rate rate = rateRepository.findById(id);
		if (rate!=null) {
			rate.setPrice(price);
			rateRepository.updatePrice(id, price);
			result = true;			
		}
		
		return result;
	}

	@Override
	public boolean deleteRate(int id) {
		return rateRepository.delete(id);
	}

	@Override
	public List<Rate> findRateByDate(int brandId, int productId, Date date) {
		List<Rate> result = rateRepository.findByDate(brandId, productId, date);
		if (result!=null) {
			for (Rate r: result) {
				r.setCurrency(currencyRepository.findByCode(r.getCurrency().getCode()));
			}
		}
		return result;
	}

}
