package io.virtualcave.example.demo.domain.repository;

import io.virtualcave.example.demo.domain.Currency;

public interface CurrencyRepository {

	public Currency findByCode(String code);
	
	
}
