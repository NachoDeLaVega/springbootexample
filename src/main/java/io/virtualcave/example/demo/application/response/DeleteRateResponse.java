package io.virtualcave.example.demo.application.response;

public class DeleteRateResponse {
	private final boolean result;
	
	public DeleteRateResponse(final boolean result) {
		this.result = result;
	}
	
	public boolean getResult() {
		return result;
	}
}
