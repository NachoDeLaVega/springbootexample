package io.virtualcave.example.demo.application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import io.virtualcave.example.demo.application.request.CreateRateRequest;
import io.virtualcave.example.demo.application.response.CreateRateResponse;
import io.virtualcave.example.demo.application.response.DeleteRateResponse;
import io.virtualcave.example.demo.application.response.FilterRateResponse;
import io.virtualcave.example.demo.application.response.GetRateResponse;
import io.virtualcave.example.demo.application.response.UpdateRateResponse;
import io.virtualcave.example.demo.domain.Rate;
import io.virtualcave.example.demo.domain.service.RateService;


@RestController
@RequestMapping("/rates")
public class RateRestMvc {
	private final RateService rateService;

    @Autowired
    public RateRestMvc(RateService rateService) {
        this.rateService = rateService;
    }
    
    @GetMapping("/holaMundo")
    public String holaMundo() {
    	return "Hola Mundo";
    }
    
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CreateRateResponse> createRate(@RequestBody final CreateRateRequest rate) {
    	
        final int id = rateService.createRate(rate.getRate());

        return ResponseEntity.status(HttpStatus.CREATED).body(new CreateRateResponse(id));
    }
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    GetRateResponse getRate(@PathVariable final int id) {
    	Rate rate = rateService.getRate(id);
    	if (rate==null) {
    		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found");
    	} 
    	return new GetRateResponse(rate); 
    }
    @PutMapping(value = "/{id}/price/{price}", produces = MediaType.APPLICATION_JSON_VALUE)
    UpdateRateResponse updatePrice(@PathVariable final int id,@PathVariable final int price) {
    	boolean result = rateService.updatePrice(id, price);
    	if (!result) {
    		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found");
    	} 
    	
    	return new UpdateRateResponse(result); 
    }
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    DeleteRateResponse delRate(@PathVariable final int id) {
    	if (!rateService.deleteRate(id)) {
    		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found");
    	} 
    	
    	return new DeleteRateResponse(true); 
    }
   
    @GetMapping(value = "/filter", produces = MediaType.APPLICATION_JSON_VALUE)
    FilterRateResponse filter(@RequestParam("date") String date, 
			@RequestParam("product_id") int product_id, 
			@RequestParam("brand_id") int brand_id) throws ParseException {
    	String formato = "yyyy-MM-dd";
    	SimpleDateFormat sdf = new SimpleDateFormat(formato);
    	
		
    	return new FilterRateResponse(rateService.findRateByDate(brand_id, product_id, sdf.parse(date)));
    }
    
    
}
