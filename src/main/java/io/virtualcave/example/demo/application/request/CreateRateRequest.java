package io.virtualcave.example.demo.application.request;


import org.springframework.lang.NonNull;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.virtualcave.example.demo.domain.Rate;

public class CreateRateRequest {
	private Rate rate;
	
	 @JsonCreator
	 public CreateRateRequest(@JsonProperty("rate") @NonNull final  Rate rate) {
		 this.rate = rate;
	 }

	 public Rate getRate() {
		 return rate;
	 }
	 
	 @Override
	 public String toString() {
		 return "[rate.brandID="+rate.getBrandId()+". Price:"+rate.getPrice()+"]";
	 }
}
