package io.virtualcave.example.demo.application.response;

import org.springframework.lang.NonNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.virtualcave.example.demo.domain.Rate;

public class GetRateResponse {
	private Rate rate;
	
	 @JsonCreator
	 public GetRateResponse(@JsonProperty("rate") @NonNull final  Rate rate) {
		 this.rate = rate;
	 }

	 public Rate getRate() {
		 return rate;
	 }
}
