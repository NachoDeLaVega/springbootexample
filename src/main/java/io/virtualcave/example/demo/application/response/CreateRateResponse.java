package io.virtualcave.example.demo.application.response;

public class CreateRateResponse {
	private final int id;
	
	public CreateRateResponse(final int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
}
