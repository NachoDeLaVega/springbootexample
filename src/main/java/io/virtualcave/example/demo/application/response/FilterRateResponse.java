package io.virtualcave.example.demo.application.response;

import java.util.List;

import org.springframework.lang.NonNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.virtualcave.example.demo.domain.Rate;

public class FilterRateResponse {
	private List<Rate> rates;
	
	 @JsonCreator
	 public FilterRateResponse(@JsonProperty("rates") @NonNull final List<Rate> rates) {
		 this.rates = rates;
	 }

	 public List<Rate> getRates() {
		 return rates;
	 }
}
