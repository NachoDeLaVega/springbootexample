package io.virtualcave.example.demo.application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import io.virtualcave.example.demo.application.request.CreateRateRequest;
import io.virtualcave.example.demo.application.response.CreateRateResponse;
import io.virtualcave.example.demo.application.response.DeleteRateResponse;
import io.virtualcave.example.demo.application.response.FilterRateResponse;
import io.virtualcave.example.demo.application.response.GetRateResponse;
import io.virtualcave.example.demo.application.response.UpdateRateResponse;
import io.virtualcave.example.demo.domain.Currency;
import io.virtualcave.example.demo.domain.Rate;
import io.virtualcave.example.demo.domain.service.RateService;


@RestController
@RequestMapping("/v1/currencies")
public class CurrencyRest {

    
    @GetMapping("/holaMundo")
    public String holaMundo() {
    	return "Hola Mundo";
    }
    
    @GetMapping(value = "/{currencyCode}", produces = MediaType.APPLICATION_JSON_VALUE)
    Currency getRate(@PathVariable final String currencyCode) {
    	Currency result = new Currency();
    	if (currencyCode.equals("EUR")) {
    		result.setCode("EUR");
    		result.setDecimals(2);
    		result.setSymbol("€");
    	} else if (currencyCode.equals("USD")) {
    		result.setCode("USD");
    		result.setDecimals(3);
    		result.setSymbol("$");    		
    	}
    	return result;
    }

}
