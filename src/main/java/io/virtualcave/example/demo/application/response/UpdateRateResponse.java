package io.virtualcave.example.demo.application.response;

public class UpdateRateResponse {
	private final boolean result;
	
	public UpdateRateResponse(final boolean result) {
		this.result = result;
	}
	
	public boolean getResult() {
		return result;
	}
}
