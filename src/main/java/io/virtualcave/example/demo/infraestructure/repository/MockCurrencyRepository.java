package io.virtualcave.example.demo.infraestructure.repository;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import io.virtualcave.example.demo.domain.Currency;
import io.virtualcave.example.demo.domain.repository.CurrencyRepository;

@Component
public class MockCurrencyRepository implements CurrencyRepository {

	@Override
	public Currency findByCode(String code) {
		Currency result = new Currency();
		result.setCode("EUR");
		result.setDecimals(2);
		result.setSymbol("€");
		return result;
	}

}
