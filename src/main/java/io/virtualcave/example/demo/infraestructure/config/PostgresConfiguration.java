package io.virtualcave.example.demo.infraestructure.config;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import io.virtualcave.example.demo.infraestructure.repository.SpringDataPostgresRatesRepository;

@EnableJpaRepositories(basePackageClasses=SpringDataPostgresRatesRepository.class)
public class PostgresConfiguration {

}
