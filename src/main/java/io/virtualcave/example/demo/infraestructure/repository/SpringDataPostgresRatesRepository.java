package io.virtualcave.example.demo.infraestructure.repository;

import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import io.virtualcave.example.demo.domain.Rate;
import io.virtualcave.example.demo.infraestructure.RateDB;

@Repository
public interface SpringDataPostgresRatesRepository extends JpaRepository<RateDB, Integer>  {
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE T_RATES SET PRICE=:newPrice WHERE id=:id", nativeQuery = true)
	int updatePrice(@Param("id") int id, @Param("newPrice")int newPrice);

	@Query(value = "SELECT ID, BRAND_ID, PRODUCT_ID, START_DATE, END_DATE, PRICE, CURRENCY_CODE "+
			" FROM T_RATES WHERE BRAND_ID=:brandId AND PRODUCT_ID=:productId AND START_DATE<=:startDate AND END_DATE>:startDate ", nativeQuery = true)
	Collection<RateDB> findByDate(@Param("brandId") int brandId, @Param("productId") int productId, @Param("startDate")Date startDate);
	
}
