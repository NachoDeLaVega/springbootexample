package io.virtualcave.example.demo.infraestructure.repository;

import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import io.virtualcave.example.demo.domain.Currency;
import io.virtualcave.example.demo.domain.repository.CurrencyRepository;
@Component
@Primary
public class UrlCurrencyRepository implements CurrencyRepository {
	@Value("${currency.url}")
	private String currencyUrl;
	
	private WebClient webClient;
	
	public UrlCurrencyRepository() {
		webClient = WebClient.create(currencyUrl);		
	}
	
	@Override
	public Currency findByCode(String code) {

		Currency result = webClient.get().uri(currencyUrl+"/"+code).retrieve().bodyToMono(Currency.class).block();
		return result;
	}

}
