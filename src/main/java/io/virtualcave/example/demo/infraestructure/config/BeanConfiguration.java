package io.virtualcave.example.demo.infraestructure.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import io.virtualcave.example.demo.domain.repository.CurrencyRepository;
import io.virtualcave.example.demo.domain.repository.RateRepository;
import io.virtualcave.example.demo.domain.service.DomainRateService;
import io.virtualcave.example.demo.domain.service.RateService;

@Configuration
@ComponentScan(basePackageClasses = DomainRateService.class)
public class BeanConfiguration {
	
    @Bean
    RateService rateService(final RateRepository rateRepository, final CurrencyRepository currencyRepository) {
        return new DomainRateService(rateRepository, currencyRepository);
    }
	
}
