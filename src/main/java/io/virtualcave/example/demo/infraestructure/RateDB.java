package io.virtualcave.example.demo.infraestructure;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.virtualcave.example.demo.domain.Currency;
import io.virtualcave.example.demo.domain.Rate;

@Entity
@Table(name = "T_RATES")
public class RateDB {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "BRAND_ID")
	private int brandId;

	@Column(name = "PRODUCT_ID")
	private int productId;
	@Column(name = "START_DATE")
	private Date startDate;
	@Column(name = "END_DATE")
	private Date endDate;
	@Column(name = "PRICE")
	private int price;
	@Column(name = "CURRENCY_CODE")
	private String currency;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBrandId() {
		return brandId;
	}
	public void setBrandId(int brandId) {
		this.brandId = brandId;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public void fromDomain(Rate rate) {
		this.id = rate.getId();
		this.brandId = rate.getBrandId();
		this.productId = rate.getProductId();
		this.startDate = rate.getStartDate();
		this.endDate = rate.getEndDate();
		this.price = rate.getPrice();
		this.currency = rate.getCurrency().getCode();
	}
	
	public Rate toDomain() {
		Rate rate = new Rate();
		rate.setId(this.id);
		rate.setBrandId(this.brandId);
		rate.setProductId(productId); 
		rate.setStartDate(this.startDate);
		rate.setEndDate(endDate);
		rate.setPrice(this.price);
		rate.getCurrency().setCode(this.currency);
		return rate;
		
	}
}
