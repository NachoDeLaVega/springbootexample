package io.virtualcave.example.demo.infraestructure.repository;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import io.virtualcave.example.demo.domain.Rate;
import io.virtualcave.example.demo.domain.repository.RateRepository;
import io.virtualcave.example.demo.infraestructure.RateDB;
@Component
@Primary
public class PostgresRatesRespository implements RateRepository {

	private final SpringDataPostgresRatesRepository sdp; 
	

    @Autowired
    public PostgresRatesRespository(final SpringDataPostgresRatesRepository sdp) {
        this.sdp = sdp;
    }

	@Override
	public Rate findById(int id) {
		Rate result=null;
		
		Optional<RateDB> r = sdp.findById(id);
		if (r.isPresent())
			result = r.get().toDomain();
		
		return result;
	}

	@Override
	public int create(Rate rate) {
		RateDB r = new RateDB();
		r.fromDomain(rate);
		return sdp.save(r).getId();
	}

	@Override
	public boolean delete(int id) {
		boolean result = false;

		try {
			sdp.deleteById(id);
			result = true;
		} catch (Exception e) {
		}
		return result;
	}

	@Override
	public boolean updatePrice(int id, int price) {
		return (sdp.updatePrice(id, price)==1);
	}

	@Override
	public List<Rate> findByDate(int brandId, int productId, Date date) {
		Collection<RateDB> result = sdp.findByDate(brandId, productId, date);
		return result.stream().map( r-> r.toDomain()).collect(Collectors.toList());

	}

}
